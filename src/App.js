import React from 'react';
import './App.css';
import { MDBDataTable } from 'mdbreact';

export const App = () => {
	const data = {};
	return (
		<div>
			<div className="row">
				<div className="col-lg-6">
					<h1>List</h1>
					<div className="row">
						<div className="col-lg-6">
							<div>
								<select className="form-control" id="roadclass">
									<option value="0">Select Road Class</option>
								</select>
							</div>
						</div>
						<div className="col-lg-6">
							<div>
								<select className="form-control" id="routeclass">
									<option value="0">Select Route Class</option>
								</select>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-lg-12">
							<MDBDataTable
								striped
								bordered
								hover
								data={data}
								id="routestable"
								className="table table-bordered table-striped"
							/>
						</div>
					</div>
				</div>
				<div className="col-lg-6">
					<h1>Map</h1>
					<div id="map" style={{ height: '600px' }} />
				</div>
			</div>
		</div>
	);
};

export default App;
